<?php

/**
 * @file
 * Helbling File Transfer hft_mail_token Entity and Controller for VBO.
 */

class HelblingFileTransferEntityController extends EntityAPIController {
  /**
   * Overrides parent::load to return only Wistia Medias with given Wistia ID.
   * @param array $ids
   * @param array $conditions
   * @return array
   */
  function load($ids = array(), $conditions = array())
  {
    // Ensure that the returned array is keyed by numeric id and ordered the
    // same as the original $ids array and remove any invalid ids.
    $return = array();
    try {
      if (!empty($ids)) {
        foreach ($ids as $name => $value) {
          $sql = db_select('file_transfer_mail_tokens', 'ftmt');
          $sql->join('file_managed', 'fm', 'ftmt.fid = fm.fid');
          $sql->fields('fm');
          $sql->fields('ftmt');
          $sql->condition('ftmt.hftid', $value, "=");
          $result = $sql->execute()->fetchObject();
          if (!empty($result)) {
            $return[$value] = $result;
          }
        }
      }
    }
    catch(Exception $e) {
      $message = t('<em>An error occured.</em>');
      drupal_set_message($message, 'error');
    }

    return $return;
  }

  /**
   * Deletes Entity from Database.
   * @param $ids
   * @param DatabaseTransaction|NULL $transaction
   * @throws Exception
   * @see $this->save()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($ids)) {
      foreach($ids as $hftid) {
        $query = db_delete('file_transfer_mail_tokens')
          ->condition('hftid', $hftid)
          ->execute();
      }
    }
  }
}


class HelblingFileTransferEntity extends Entity {

  protected $entityType = 'hft_mail_token';

  /**
   * Returns info about the properties of this entity, as declared in hook_entity_property_info().
   *
   * @return array
   *   The properties defined for this entity.
   *
   * @see picasa_entity_property_info()
   */
  public function properties() {
    $info = entity_get_property_info($this->entityType());
    return $info['properties'];
  }

}