<?php
/**
 * @file
 * File Compression Callback for 'compress_transfer_file/%/%'.
 * Simulating Async File Compression.
 */

require_once "helbling_file_transfer.tools.inc";

/**
 * "Asynchronous" function called by compression Batch via Socket.
 * @param $zipname_base64
 * @param $filename_base64
 * @internal param $zipname
 * @internal param $filename
 */
function _helbling_file_transfer_compress($zipname_base64, $filename_base64) {
    set_time_limit(0);
    $zipname = base64_decode($zipname_base64);
    $filename = base64_decode($filename_base64);
    ob_implicit_flush();
    if (!$zip_file = helbling_file_transfer_open_zip($zipname)) {
      print "1";
    } elseif (!empty($zipname) &&
      !empty($filename) &&
      file_exists($filename)) {
      print "0";
      $size = filesize($filename);
      if ($size === FALSE || $size >= 2*1024*1024*8 || is_archive($filename)) {
        $compressed = $zip_file->add($filename,
          PCLZIP_OPT_NO_COMPRESSION,
          PCLZIP_OPT_REMOVE_ALL_PATH,
          PCLZIP_OPT_ADD_TEMP_FILE_ON);
      }
      else {
        $compressed = $zip_file->add($filename,
          PCLZIP_OPT_REMOVE_ALL_PATH,
          PCLZIP_OPT_ADD_TEMP_FILE_ON);
      }
      if ($compressed == 0) {
        print "1";
      }
      else {
        print "3";
      }
    }
    else {
      print "1";
    }
}