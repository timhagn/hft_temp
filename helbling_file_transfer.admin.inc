<?php
/**
 * @file
 * Admin Screen for the Helbling File Transfer Module.
 */

/**
 * Callback for the UDB-REST config screen at /admin/config/media/file_transfer
 * @param $form
 * @param $form_state
 * @return mixed
 */
function _helbling_file_transfer_config_form($form, &$form_state) {
  $message = '<p>' . t('This is the File Transfer Mailer Configuration Screen') . '</p>';
  drupal_set_message($message);

  $default_ext = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp zip tar gz mp4 mkv avi docx xlsx';

  $expires = variable_get('file_transfer_expires', '0');
  $validate_extensions = variable_get('file_transfer_validate_extensions',
    $default_ext);
  $auth = variable_get('file_transfer_authentification', '');

  $form['file_transfer_expires'] = array(
    '#type' => 'textfield',
    '#title' => t('When shall mailed file transfer links expire? 0 for unlimited, D (days), W (weeks), M (months).'),
    '#default_value' => $expires?$expires:'0',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['file_transfer_validate_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions. Delimiter is space.'),
    '#default_value' => $validate_extensions?$validate_extensions:$default_ext,
    '#size' => 128,
    '#maxlength' => 255,
  );

  $form['file_transfer_authentification'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter Basic Authentification, if used, as <em>username:password</em>s'),
    '#default_value' => $auth?$auth:'',
    '#size' => 128,
    '#maxlength' => 255,
  );

  return system_settings_form($form);
}