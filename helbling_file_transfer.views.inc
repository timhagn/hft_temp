<?php
/**
 * @file
 * Sets View settings for file_transfer_mail_token Schema.
 */

/**
 * Implements hook_views_data().
 */
function helbling_file_transfer_views_data()
{
  $data = array(
    'file_transfer_mail_tokens' => array(
      'table' => array(
        'group' => t('File Transfer Mailer Sent Files'),
        'base' => array(
          'field' => 'hftid', //Primary key
          'title' => t('File Transfer Mailer List'),
          'help' => t('File Transfer Mailer Database Table'),
        ),
        'entity type' => 'hft_mail_token',
        'join' => array(
          'file_transfer_mail_tokens' => array(
            'left_table' => 'file_managed',
            'left_field' => 'fid',
            'field' => 'fid',
            'type' => 'LEFT',
          ),
          'file_managed' => array(
            'left_field' => 'fid',
            'field' => 'fid',
            'type' => 'LEFT',
          ),
        ),
      ),
      'hftid' => array(
        'title' => t('File Transfer Mailer ID'),
        'help' => t('File Transfer Mailer record ID.'),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
      ),
      'mail_recipients' => array(
        'title' => t('Mail Recipients'),
        'help' => t('File Transfer Mailer Recipients.'),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
      ),
      'mail_body' => array(
        'title' => t('Mail Body'),
        'help' => t('File Transfer Mailer Mail Body.'),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
      ),
      'fid' => array(
        'title' => t('File ID'),
        'help' => t('File Transfer Mailer File Managed ID.'),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
      ),
      'expires' => array(
        'title' => t('Expiry Date'),
        'help' => t('The date this record expires.'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),
    ),
  );
  return $data;
}