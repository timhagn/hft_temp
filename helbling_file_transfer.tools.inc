<?php
/**
 * @file
 * Some tools helping in Batch processing and Configuration.
 */

/**
 * Queries the File Transfer variable 'file_transfer_expires',
 * which either has a value of '0' meaning no expiry time or
 * a string in the form 1Y2M3D, which gets translated by
 * stringtotime() into a timestamp from now.
 * @return mixed Returns FALSE on failure, expiry string or timestamp on success
 */
function getExpiration() {
  $expiry_string = trim(variable_get('file_transfer_expires', '0'));
  if ($expiry_string === '0') {
    return intval($expiry_string);
  }
  else {
    $expiry_string = strtolower(str_replace(' ', '', $expiry_string));
    $expiry_string = str_replace('y', ' year', $expiry_string);
    $expiry_string = str_replace('m', ' month', $expiry_string);
    $expiry_string = str_replace('d', ' day', $expiry_string);

    if (($timestamp = strtotime("+ " . $expiry_string)) === FALSE){
      return FALSE;
    } else {
      //return "+ $expiry_string == " . date('l dS \o\f F Y h:i:s A', $timestamp);
      return $timestamp;
    }
  }
}

/**
 * Checks if PClZip is installed and creates a temporary ZIP-File.
 * @param $destination
 * @return PclZip
 */
function helbling_file_transfer_open_zip($destination = '', $open_temp = TRUE)
{
  $temporary_scheme = 'temporary://';
  $tempdir = '';
  if (strpos($destination, DIRECTORY_SEPARATOR) !== FALSE) {
    $tempdir = substr($destination, 0, strrpos($destination, DIRECTORY_SEPARATOR) + 1);
  }
  if (!class_exists('PclZip')) {
    $lib_path = libraries_get_path('pclzip');
    if (!is_dir($lib_path)) {
      drupal_set_message('You need to download PclZip at <a href="http://www.phpconcept.net/pclzip/pclzip-downloads">http://www.phpconcept.net/pclzip/pclzip-downloads</a> and extract content in /sites/all/libraries/pclzip in order to use the Download module.', 'warning');
    }
    // Change Separator from ' ' to ';' (important for windows files)
    define('PCLZIP_SEPARATOR', ';');
    // Change Temporary Directory to default Drupal-Directory (important for larger files)
    define('PCLZIP_TEMPORARY_DIR', drupal_realpath($temporary_scheme . $tempdir)
      . DIRECTORY_SEPARATOR);
    //watchdog(WATCHDOG_DEBUG,'PCLZIP_TEMPORARY_DIR: ' . PCLZIP_TEMPORARY_DIR);
    include_once DRUPAL_ROOT . '/' . $lib_path . '/pclzip.lib.php';
  }
  // Create the Archive
  if (empty($destination)) {
    return null;
  } else {
    if ($open_temp) {
      return new PclZip(drupal_realpath($temporary_scheme . $destination) . '.zip');
    }
    else {
      return new PclZip($destination);
    }
  }
}


/**
 * This function checks if the "async" function _compress has already
 * put the file into the zip archive.
 * @param $zipname
 * @param $filename
 * @return int
 */
function _helbling_file_transfer_check_archive($zipname, $filename) {
  $filename = substr($filename, strrpos($filename, DIRECTORY_SEPARATOR) + 1);
  if (!$zip_file = helbling_file_transfer_open_zip($zipname)) {
    return 1;
  } elseif (!empty($zipname) && !empty($filename)) {
    $list = $zip_file->listContent();
    $isarchived = FALSE;
    if (is_array($list)) {
      foreach ($list as $key => $listitem) {
        if ($listitem['stored_filename'] == $filename) {
          $isarchived = $listitem;
        } else {
          continue;
        }
      }
    }
    if ($isarchived) {
      if (strtolower($isarchived['status']) !== 'ok') {
        return 1;
      }
      else {
        return 3;
      }
    }
    else {
      return 2;
    }
  }
  return 1;
}

/**
 * Checks the Archive Size to approximate Compression Progress.
 * @param $directory
 * @return bool|int
 */
function helbling_file_transfer_archive_size($directory) {
  $temporary_scheme = 'temporary://';
  $tempfile = glob(drupal_realpath($temporary_scheme . $directory)
    . DIRECTORY_SEPARATOR .'pclzip-*');
  if (isset($tempfile[0]) && file_exists($tempfile[0])) {
    return filesize($tempfile[0]);
  }
  else {
    return FALSE;
  }
}


/**
 * Tries to find out, if a given file is an archive
 * @param $filename
 * @return bool
 */
function is_archive($filename) {
  if (!$mime = file_get_mimetype($filename)) {
    $temp = explode(".", $_FILES["uploaded"]["name"]);
    $archiveExt = array('bz2', 'gz', '7z', 'arj', 'dmg', 'lzh', 'lha', 'rar', 'tar', 'zip');
    $extension = end($temp);
    if (in_array($extension, $archiveExt)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
    $archiveMimes = array(
      'application/x-bzip2',
      'application/gzip',
      'application/x-7z-compressed',
      'application/x-arj',
      'application/x-apple-diskimage',
      'application/x-lzh',
      'application/x-rar-compressed',
      'application/x-gtar',
      'application/zip',
    );
    if (in_array($mime, $archiveMimes)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
}


/**
 * Generates a (nearly secure ; ) Token String with the best available function.
 * @param int $length
 * @return string
 */
function random_token($length = 32)
{
  if (!isset($length) || intval($length) <= 8) {
    $length = 32;
  }
  if (function_exists('random_bytes')) {
    return substr(bin2hex(random_bytes($length)), 0, $length);
  }
  if (function_exists('mcrypt_create_iv')) {
    return substr(bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM)), 0, $length);
  }
  if (function_exists('openssl_random_pseudo_bytes')) {
    return substr(bin2hex(openssl_random_pseudo_bytes($length)), 0, $length);
  }
  return FALSE;
}



/**
 * Fixes an Class Object if $object is __PHP_Incomplete_Class.
 * @param $object
 * @return mixed
 */
function fixThisObject(&$object)
{
  // Only to include PclZip Class Object if not already loaded.
  open_zip();

  if (!is_object ($object) && gettype ($object) == 'object')
    return ($object = unserialize (serialize ($object)));
  return $object;
}