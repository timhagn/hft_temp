<?php
/**
 * @file
 * Default View for File Transfer Mailer.
 */

/**
 * Implements hook_views_default_views().
 */
function helbling_file_transfer_views_default_views()
{
  $view = new view();
  $view->name = 'file_transfer_mailer_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'file_transfer_mail_tokens';
  $view->human_name = 'File Transfer Mailer List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'hftid' => 'hftid',
    'fid' => 'fid',
    'mail_recipients' => 'mail_recipients',
    'mail_body' => 'mail_body',
    'expires' => 'expires',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hftid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'fid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'mail_recipients' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'mail_body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expires' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Bulk operations: File Transfer Mailer Sent Files */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::helbling_file_transfer_delete_hft_file' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::helbling_file_transfer_resend' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: File Transfer Mailer Sent Files: File Transfer Mailer ID */
  $handler->display->display_options['fields']['hftid']['id'] = 'hftid';
  $handler->display->display_options['fields']['hftid']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['hftid']['field'] = 'hftid';
  /* Field: File Transfer Mailer Sent Files: File ID */
  $handler->display->display_options['fields']['fid']['id'] = 'fid';
  $handler->display->display_options['fields']['fid']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['fid']['field'] = 'fid';
  /* Field: File Transfer Mailer Sent Files: Mail Recipients */
  $handler->display->display_options['fields']['mail_recipients']['id'] = 'mail_recipients';
  $handler->display->display_options['fields']['mail_recipients']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['mail_recipients']['field'] = 'mail_recipients';
  /* Field: File Transfer Mailer Sent Files: Mail Body */
  $handler->display->display_options['fields']['mail_body']['id'] = 'mail_body';
  $handler->display->display_options['fields']['mail_body']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['mail_body']['field'] = 'mail_body';
  /* Field: File Transfer Mailer Sent Files: Expiry Date */
  $handler->display->display_options['fields']['expires']['id'] = 'expires';
  $handler->display->display_options['fields']['expires']['table'] = 'file_transfer_mail_tokens';
  $handler->display->display_options['fields']['expires']['field'] = 'expires';
  $handler->display->display_options['fields']['expires']['empty'] = 'Unlimited';
  $handler->display->display_options['fields']['expires']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['expires']['date_format'] = 'short';
  $handler->display->display_options['fields']['expires']['second_date_format'] = 'long';
  $translatables['file_transfer_mailer_list'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('File Transfer Mailer Sent Files'),
    t('- Choose an operation -'),
    t('File Transfer Mailer ID'),
    t('File ID'),
    t('Mail Recipients'),
    t('Mail Body'),
    t('Expiry Date'),
    t('Unlimited'),
  );

  $views[$view->name] = $view;
  return $views;
}