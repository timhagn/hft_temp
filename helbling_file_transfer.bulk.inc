<?php
/**
 * @file
 * Helbling File Transfer Bulk Screens for Archive Compression.
 */

require_once "helbling_file_transfer.tools.inc";

/**
 * Batch API callback
 * @param $plupload_files
 * @param $data
 */
function _helbling_file_transfer_compress_bulk($plupload_files, $data)
{
  if (!$zip_file = helbling_file_transfer_open_zip($data['temporary_dir'] . '/' . $data['temporary_file'])) {
    drupal_set_message(t('There was an error creating the Upload Archive!'));
  } elseif (is_array($plupload_files)) {
    $operations = array();

    foreach ($plupload_files as $key => $upfile) {
      if ($upfile['status'] === 'done') {
        $nextfile = '';
        if (isset($plupload_files[$key+1])) {
          $nextfile = $plupload_files[$key+1]['name'];
        }
        $operations[] = array('_helbling_file_transfer_process_file',
          array($upfile, $data['temporary_dir'] . '/' . $data['temporary_file'], $nextfile, $data));
      }
    }

    $batch = array(
      'operations' => $operations,
      'finished' => '_helbling_file_transfer_batch_finished',
      'title' => t('Compressing Uploaded Files'),
      'init_message' => t('Now compressing: %filename',
        array('%filename' => $plupload_files[0]['name'])),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Error while Compressing uploaded Files.')
    );
    return $batch;
  }
  return array();
}


/**
 * Batch 'process' callback
 * Compresses each file by calling the "async" function _compress.
 * @param $upfile
 * @param $zipname
 * @param $nextfile
 * @param $data
 * @param $context
 */
function _helbling_file_transfer_process_file($upfile, $zipname, $nextfile, $data, &$context) {
global $base_url, $base_path;
  $temporary_scheme = 'temporary://';
  $context['finished'] = 0;
  if ($upfile['status'] === 'done') {
    $context['results']['zipname'] = $temporary_scheme . $zipname;
    $context['results']['data'] = $data;
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = filesize(drupal_realpath($upfile['tmppath']));
    }
    if ($context['sandbox']['progress'] == 0)
    {
      $source = $upfile['tmppath'];
      $destination = file_stream_wrapper_uri_normalize($temporary_scheme
        . $data['temporary_dir']
        . DIRECTORY_SEPARATOR . $upfile['name']);
      if (file_exists(drupal_realpath($destination))) {
        file_unmanaged_delete(drupal_realpath($destination));
      }
      $destination = file_unmanaged_move($source, $destination, FILE_EXISTS_RENAME);
      if ($destination) {
        $context['sandbox']['destination'] = $destination;

        $socket = new SocketFork($base_url . $base_path . 'compress_transfer_file/'
          . urlencode(base64_encode($zipname)) . '/'
          . urlencode(base64_encode(drupal_realpath($destination)))
        );
        $response = _helbling_file_transfer_check_archive($zipname,
          drupal_realpath($destination));
        if (in_array(intval($response), [2,3])) {
          unset($context['error']);
          if ($zip_tmp_size = helbling_file_transfer_archive_size($data['temporary_dir'])) {
            $context['sandbox']['progress'] = $zip_tmp_size;
          }
          else {
            $context['sandbox']['progress']++;
          }
        }
        checkResponse($response, $context, $upfile, $nextfile, $data);
      } else {
        $context['finished'] = 1;
        $context['error'] = 1;
        $context['message'] = t('There was an error compressing: %filename', array('%filename' => $upfile['name']));
      }
    }
    else {
      $response = _helbling_file_transfer_check_archive($zipname,
        drupal_realpath($context['sandbox']['destination']));
      if (intval($response) === 2) {
        unset($context['error']);
        if ($zip_tmp_size = helbling_file_transfer_archive_size($data['temporary_dir'])) {
          $context['sandbox']['progress'] = $zip_tmp_size;
        }
        else {
          $context['sandbox']['progress'] += $context['sandbox']['max'] / 10000;
          if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
            $context['sandbox']['progress'] = 0.5 * $context['sandbox']['max'];
          }
        }
      }
      else {
        checkResponse($response, $context, $upfile, $nextfile, $data);
      }
    }
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if (!isset($context['error']) &&
      $context['finished'] != 1 &&
      $context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }
}


/**
 * Checks Archive Response and sets $context accordingly.
 * @param $response
 * @param $context
 * @param $upfile
 * @param $nextfile
 * @param $data
 */
function checkResponse($response, &$context, $upfile, $nextfile, $data) {
  if ($response === FALSE || intval($response) === 1) {
    unset($context['sandbox']['progress']);
    unset($context['sandbox']['max']);
    $context['finished'] = 1;
    $context['error'] = 1;
    $context['message'] = t('There was an error compressing: %filename', array('%filename' => $upfile['name']));
  }
  elseif (intval($response) === 3) {
    $context['results']['file'][] = $upfile['name'];
    // Update our progress information.
    if (empty($nextfile)) {
      $context['message'] = t('Now mailing to: %recipients', array('%recipients' => $data['recipients']));
    } else {
      $context['message'] = t('Now compressing: %filename', array('%filename' => $nextfile));
    }
    file_unmanaged_delete($context['sandbox']['destination']);
    $context['finished'] = 1;
  }
}


/**
 * Batch 'finished' callback
 * @param $success
 * @param $results
 * @param $operations
 * @throws Exception
 */
function _helbling_file_transfer_batch_finished($success, $results, $operations)
{
  if ($success && isset($results['file'])) {
    $message = t('@count items successfully compressed!',
      array('@count' => count($results['file']))
    );
    $message .= theme('item_list', array('items' => $results['file']));

    drupal_set_message($message);

  $_SESSION['results'] = $results;
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while compressing %error_operation with arguments: @arguments',
      array('%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      )
    );
    drupal_set_message($message, 'error');
  }
}


/**
 * Batch API callback
 * @param $plupload_files
 * @param $data
 */
function _helbling_file_transfer_mail_bulk($data)
{
  $data = (array) $data;
  if ($data['temporary_dir'] &&
    $data['temporary_file'] &&
    $data['recipients'] &&
    $data['return_uri']) {
    $operations[] = array('_helbling_file_transfer_save_file',
      array($data['temporary_dir'] . DIRECTORY_SEPARATOR .$data['temporary_file'],
        $data));

    $recipient_array = explode(',', $data['recipients']);
    foreach ($recipient_array as $key => $recipient) {
      $nextrecipient = '';
      if (isset($recipient_array[$key+1])) {
        $nextrecipient = $recipient_array[$key+1];
      }
      $operations[] = array('_helbling_file_transfer_mail',
        array($recipient, $nextrecipient, $data));
    }

    $batch = array(
      'operations' => $operations,
      'finished' => '_helbling_file_transfer_batch_save_and_mail_finished',
      'title' => t('Saving and Mailing'),
      'init_message' => t('Now saving compressed file to DB.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Error while Compressing uploaded Files.')
    );

    return $batch;
  }
  return array();
}

/**
 * Batch Processing function. Saves hft_mail_token entity.
 * @param $filenamepath
 * @param $data
 * @param $context
 */
function _helbling_file_transfer_save_file($filenamepath, $data, &$context) {
  $private_scheme = 'private://';
  $temporary_scheme = 'temporary://';
  $context['results']['temporary_dir'] = $data['temporary_dir'];
  if (strpos($filenamepath, '.zip') === FALSE) {
    $filenamepath = drupal_realpath($temporary_scheme . $filenamepath . '.zip');
  }

  if ($filenamepath && $data['recipients'] && file_exists($filenamepath)) {
    $filename = substr($filenamepath, strrpos($filenamepath, DIRECTORY_SEPARATOR) + 1);
    $destination = file_stream_wrapper_uri_normalize($private_scheme . $filename);
    $destination = file_unmanaged_move($filenamepath, $destination, FILE_EXISTS_RENAME);
    $file = plupload_file_uri_to_object($destination);
    $file_result = file_save($file);
    if ($file_result) {
      $mail_token = random_token();
      $expires = (int) getExpiration();
      $fields = array(
        'mail_token' => $mail_token,
        'mail_recipients' => $data['recipients'],
        'fid' => $file_result->fid,
        'expires' => $expires,
      );
      if (!empty($data['mail_body'])) {
        $fields['mail_body'] = $data['mail_body'];
      }
      $hft_mail_token = entity_create('hft_mail_token', $fields);
      $result = entity_save('hft_mail_token', $hft_mail_token);
      if ($result && isset($hft_mail_token->hftid)) {
        $hftid = $hft_mail_token->hftid;
        file_usage_add($file_result, 'helbling_file_transfer', 'hft', $hftid);
        $context['results']['hftid'] = $hftid;
        $context['results']['filename'] = $filename;
        $context['results']['mail_token'] = $mail_token;
        $context['message'] = t('Now mailing to: %recipients', array('%recipients' => $data['recipients']));
        $context['finished'] = 1;
        $_SESSION['results']['hftid'] = $hftid;
      }
      else {
        $context['finished'] = 1;
        $context['error'] = 1;
        $context['message'] = t('There was an error saving File Transfer Data into DB!');
      }
    }
    else {
      $context['finished'] = 1;
      $context['error'] = 1;
      $context['message'] = t('There was an error saving the compressed file: %filename!', array('%filename' => $filename));
    }
  }
}

/**
 * Batch Processing function. Mails Link to given Recipient.
 * @param $recipient
 * @param $nextrecipient
 * @param $data
 * @param $context
 */
function _helbling_file_transfer_mail($recipient, $nextrecipient, $data, &$context) {
global $user, $base_url, $base_path;
  if ($recipient &&
      $context['results']['hftid'] &&
      $context['results']['filename'] &&
      $context['results']['mail_token']) {
    $link = t('<a href="' . $base_url . $base_path
      . 'get_transfer_file?mail=' . $recipient . '&ftok=' . $context['results']['mail_token']
      . '">Click to Download: ' . $context['results']['filename'] . '</a>');
    $mail_body = $data['mail_body'];
    $error = '';
    try {
      rules_invoke_event('hft_mail_event', $user, $link, $recipient, $mail_body);
    } catch (Exception $e) {
      $error = t('Caught exception: %errormsg', array('%errormsg', $e->getMessage()));
    }
    if (empty($error)) {
      $context['results']['recipients'][] = $recipient;
      $context['message'] = t('Now mailing to: %nextrecipient',
        array('%nextrecipient' => $nextrecipient ? $nextrecipient : 'Mailing finished.'));
      $context['finished'] = 1;
    }
    else {
      $context['finished'] = 1;
      $context['error'] = 1;
      $context['message'] = $error;
    }
  }
  else {
    $context['finished'] = 1;
    $context['error'] = 1;
    $context['message'] = t('There was an error mailing to: %recipient', array('%recipient' => $recipient));
  }
}


/**
 * Batch 'finished' callback
 * @param $success
 * @param $results
 * @param $operations
 * @throws Exception
 */
function _helbling_file_transfer_batch_save_and_mail_finished($success, $results, $operations)
{
  $temporary_scheme = 'temporary://';
  if ($success &&
      $results['hftid'] &&
      $results['filename'] &&
      $results['mail_token'] &&
      $results['temporary_dir']) {
    $message = t('Mailed successfully to @count Addresses.',
      array('@count' => count($results['recipients']))
    );
    $message .= theme('item_list', array('items' => $results['recipients']));

    array_map('unlink', glob(drupal_realpath($temporary_scheme
        . $results['temporary_dir']) .
      DIRECTORY_SEPARATOR . '*'));
    drupal_rmdir(drupal_realpath($temporary_scheme . $results['temporary_dir']));

    drupal_set_message($message);
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments',
      array('%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      )
    );
    drupal_set_message($message, 'error');
  }
}



/*
 * This class is a quick implementation for a "Async" PHP call.
 */
class SocketFork
{
  protected $socket;
  private $url_info;

  /**
   * SocketFork constructor. Creates Socket Stream and calls "Async" function.
   * @param $request
   */
  public function __construct($request)
  {
    if (!$url_info = parse_url($request)) {
      return false;
    }

    switch ($url_info['scheme']) {
      case 'https':
        $scheme = 'ssl://';
        $port = ':443';
        $context = stream_context_create();
        $result = stream_context_set_option($context, 'ssl', 'verify_host', false);
        $result = stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
        $this->socket = stream_socket_client($scheme . $url_info['host'] . $port, $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $context);
        break;
      case 'http':
      default:
        $scheme = 'tcp://';
        $port = ':80';
        $this->socket = stream_socket_client($scheme . $url_info['host'] . $port, $errno, $errstr);
        break;
    }
    if ($this->socket) {
      stream_set_blocking($this->socket, FALSE);
      $url_info['request'] = $request;
      $this->url_info = $url_info;
      $this->getAsync();
    }
    else {
      watchdog('error', 'Socket error... ' . $this->socket);
    }
    return $this;
  }

  /**
   * Starts the "Async" Call
   */
  private function getAsync() {
    $auth = variable_get('file_transfer_authentification', '');
    if (!empty($auth)) {
      fwrite($this->socket, "GET " . $this->url_info['path']
        . " HTTP/1.0\r\nHost: " . $this->url_info['host']
        . "\r\nAuthorization: Basic " . base64_encode($auth)
        . "\r\nAccept: */*\r\n\r\n");
    }
    else {
      fwrite($this->socket, "GET " . $this->url_info['path']
        . " HTTP/1.0\r\nHost: " . $this->url_info['host']
        ."\r\nAccept: */*\r\n\r\n");
    }
  }
}



