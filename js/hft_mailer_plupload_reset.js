(function ($) {
    Drupal.behaviors.hft_mailer_plupload_reset = {
        attach: function (context, settings) {
            // Get the Plupload object.
            $('#helbling_file_transfer_upload').once('file-transfer-plupload', function() {
                var uploader = $(this).pluploadQueue();

                // Add previous files to uploader (flag as uploaded, as they already were).
                for (var i = 0; i < Drupal.settings.hft_mailer_plupload_files.length; i++) {
                    var file = Drupal.settings.hft_mailer_plupload_files[i];
                    var new_file = new plupload.File(file.id, file.name, file.size);

                    // Add extra data to the file to say it's already uploaded (which it is, since it was passed in from form_state).
                    new_file.status = plupload.DONE;
                    new_file.loaded = new_file.size;
                    new_file.percent = 100;

                    // Push the file into the uploader's file list.
                    uploader.files.push(new_file);

                    // Trigger that an upload happened so that form_state gets properly filled.
                    uploader.trigger("UploadFile", new_file);
                }

                // Update the uploader to reflect the new files.
                if (uploader.files.length > 0) {
                    var files_loaded = 0;

                    // Add up the total bytes that were uploaded.
                    for (var i = 0; i < uploader.files.length; i++) {
                        files_loaded += uploader.files[i].loaded;
                    }

                    // Update the queue progress to reflect the files that are updated.
                    var queueprogress = uploader.total;
                    queueprogress.size = files_loaded;
                    queueprogress.loaded = files_loaded;
                    queueprogress.uploaded = uploader.files.length;
                    queueprogress.percent = 100;

                    // Refresh the uploader so it shows the new files and percentages.
                    uploader.trigger("QueueChanged");
                    uploader.refresh();
                }
            });
        }
    };
})(jQuery);