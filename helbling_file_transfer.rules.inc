<?php
/**
 * @file
 * Rules Configuration and Creation functions.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function helbling_file_transfer_default_rules_configuration()
{
  $config = array();
  $rule = '{ "rules_send_mail_with_download_link" :
    {
      "LABEL" : "Send Mail with Download Link",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "helbling_file_transfer" ],
      "ON" : { "hft_mail_event" : [] },
      "DO" : [
        { "mail" : {
            "to" : "[recipient:value]",
            "subject" : "User [current-user:name] sent you a Download Link from [site:name]",
            "message" : "[mail_body:value]\r\nUser [current-user:name] sent you following Download Link from [site:name]:\r\n[link:value]\r\nClick to Download!",
            "language" : [ "current-user:language" ]
          }
        }
      ]
    }
  }';

  $config['rules_send_mail_with_download_link'] = rules_import($rule);
  return $config;
}


/**
 * Implements hook_rules_event_info().
 */
function helbling_file_transfer_rules_event_info()
{
  $rule_event['hft_mail_event'] = array(
    'label' => t('Send Mail with Download Link'),
    'module' => 'helbling_file_transfer',
    'group' => 'Helbling File Transfer Mail',
    'variables' => array(
      'current_user' => array(
        'type' => 'user',
        'label' => t('The current logged in user.')
      ),
      'link' => array(
        'type' => 'text',
        'label' => t('The link to be sent.')
      ),
      'recipient' => array(
        'type' => 'text',
        'label' => t('The E-Mail Recipient.')
      ),
      'mail_body' => array(
        'type' => 'text',
        'label' => t('The E-Mail Body.')
      ),
    ),
  );

  return $rule_event;
}